//
//  TutorialOneViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit

class TutorialOneViewController: UIViewController {

    class func instantiateFromStoryboard() -> TutorialOneViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialOneViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UserDefaults.standard.set("true", forKey: "isFirstLaunched")
//        self.initialSetup()
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
        self.DarkStatusBar()
        
    }

    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        let vc = TutorialTwoViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func nextAction(_ sender : UIButton) {
        let vc = TutorialTwoViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func skipAction(_ sender : UIButton) {
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
