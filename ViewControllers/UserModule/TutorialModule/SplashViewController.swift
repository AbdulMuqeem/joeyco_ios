//
//  SplashViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit

class SplashViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SplashViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SplashViewController
    }
    
    @IBOutlet weak var imgLogo:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            self.animateLogo()
        })
        
    }
    
    fileprivate func animateLogo() {
        
        UIView.animate(withDuration: 0.5, animations: {
            [weak self] in
            guard let self = self else {
                return
            }
            self.imgLogo.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        }) {  (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                [weak self] in
                guard let self = self else { return
                }
                self.imgLogo.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }) { [weak self] (finished) in
                guard let _ = self else { return }
                
                if finished {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                        
                        if #available(iOS 13.0, *) {
                            SceneDelegate.getInstatnce().navigateTOInitialViewController()
                        }
                        else {
                            AppDelegate.getInstatnce().navigateTOInitialViewController()
                        }
                        
                    })
                }
                
            }
        }
    }
    
    
}
