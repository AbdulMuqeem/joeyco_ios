//
//  HomeViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit
import GoogleMaps

extension HomeViewController: AlertViewDelegate , AlertViewDelegateAction {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func okButtonAction() {
        let nav = RootViewController.instantiateFromStoryboard()
        
        if #available(iOS 13.0, *) {
            SceneDelegate.getInstatnce().window?.rootViewController = nav
        } else {
            AppDelegate.getInstatnce().window?.rootViewController = nav
        }
        
        let VC = LoginViewController.instantiateFromStoryboard()
        nav.pushViewController(VC, animated: true)
    }
    
    func cancelAction() {
        self.alertView.isHidden = true
    }
    
}

extension HomeViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your location to show your current location") { (sender) in
                    
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        }
        
    }
    
}


class HomeViewController: UIViewController {

    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var onlineView:UIView!
    @IBOutlet weak var offlineView:UIView!
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    var isLogout:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.onlineView.roundCorners(corners: [.topLeft , .bottomLeft], radius: 20)
        self.offlineView.roundCorners(corners: [.topRight , .bottomRight], radius: 20)
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetup() {
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        self.alertView.delegateAction = self

        self.mapView.delegate = self
//        self.didAccesToLocation()
//        self.determineMyCurrentLocation()
        
        self.getMap(33.7295, 73.0372)
        
        if self.isLogout == true {
            
            DispatchQueue.main.async {
                
                self.alertView.btnOkAction.setTitle("Logout", for: .normal)
                self.alertView.btnCancel.setTitle("Cancel", for: .normal)
                
                self.alertView.alertShow(title: "Alert", msg: "Are you sure you want to Logout ?", id: 1)
                self.alertView.isHidden = false
                
            }
        }
    }
    
    @IBAction func sideMenuAction(_ sender : UIButton) {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func onlineAction(_ sender : UIButton) {
        self.onlineView.isHidden = false
        self.offlineView.isHidden = true
    }
    
    @IBAction func offlineAction(_ sender : UIButton) {
        self.onlineView.isHidden = true
        self.offlineView.isHidden = false
    }
    
}
