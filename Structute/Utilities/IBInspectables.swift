//
//  IBInspectables.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}



@IBDesignable
extension UILabel {
    
    @IBInspectable var FontSize: CGFloat {
        get {
            return self.FontSize
        }
        set {
            self.font = self.getFontWithAdjustedSize(size:newValue)
        }
    }
    
    func getFontWithAdjustedSize(size:CGFloat)-> UIFont{
        let font:UIFont? = UIFont(name:self.font.fontName , size: GISTUtility.convertToRatio(size)) //SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: size))
        return font!
    }
    
}

@IBDesignable
extension UIButton {
    
    @IBInspectable var FontSize: CGFloat {
        get {
            return self.FontSize
        }
        set {
            self.titleLabel!.font = self.getFontWithAdjustedSize(size:newValue)
        }
    }
    
    func getFontWithAdjustedSize(size:CGFloat)-> UIFont{
        let font:UIFont? = UIFont(name:self.titleLabel!.font.fontName , size: GISTUtility.convertToRatio(size))
        return font!
    }
    
}

@IBDesignable
extension UITextField {
    
    @IBInspectable var FontSize: CGFloat {
        get {
            return self.FontSize
        }
        set {
            self.font = self.getFontWithAdjustedSize(size:newValue)
        }
    }
    
    func getFontWithAdjustedSize(size:CGFloat)-> UIFont{
        let font:UIFont? = UIFont(name:self.font!.fontName , size: GISTUtility.convertToRatio(size))
        return font!
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
}

class SizeUtil {
    
    static let sharedInstance: SizeUtil = SizeUtil()
    
    private init() {
        print("AAA");
    }
    class func convertIphone6SizeToOtherPhonesRespectively(size : CGFloat) -> CGFloat{
        
        /*
         Resize View to thier Canvas area
         */
        
        var tempSize = size;
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 480:
                //print("iPhone Classic")
                tempSize = ( size * 72.3 ) / 100 ;
            case 960:
                // print("iPhone 4 or 4S")
                tempSize = ( size * 72.3 ) / 100 ;
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                tempSize = ( size * 85.3 ) / 100 ;
            case 1334:
                //print("iPhone 6 or 6S")
                tempSize = size;
            case 2208:
                // print("iPhone 6+ or 6S+")
                tempSize = ( size * 110.47 ) / 100 ;
            default:
                break
            // print("other", terminator: "")
            }
        }
        
        
        return tempSize
    }
    
}

extension UIScreen {
    
    var isPhone4: Bool {
        
        return self.nativeBounds.size.height == 960;
    }
    
    var isPhone5: Bool {
        return self.nativeBounds.size.height == 1136;
    }
    
    var isPhone6: Bool {
        return self.nativeBounds.size.height == 1334;
    }
    
    var isPhone6Plus: Bool {
        return self.nativeBounds.size.height == 2208;
    }
    
}

extension UIImage {
    var isPortrait:  Bool    { size.height > size.width }
    var isLandscape: Bool    { size.width > size.height }
    var breadth:     CGFloat { min(size.width, size.height) }
    var breadthSize: CGSize  { .init(width: breadth, height: breadth) }
    var breadthRect: CGRect  { .init(origin: .zero, size: breadthSize) }
    var circleMasked: UIImage? {
        guard let cgImage = cgImage?
                .cropping(to: .init(origin: .init(x: isLandscape ? ((size.width-size.height)/2).rounded(.down) : 0,
                                                  y: isPortrait  ? ((size.height-size.width)/2).rounded(.down) : 0),
                                    size: breadthSize)) else { return nil }
        let format = imageRendererFormat
        format.opaque = false
        return UIGraphicsImageRenderer(size: breadthSize, format: format).image { _ in
            UIBezierPath(ovalIn: breadthRect).addClip()
            UIImage(cgImage: cgImage, scale: 1, orientation: imageOrientation)
                .draw(in: .init(origin: .zero, size: breadthSize))
        }
    }
}

extension UIView {
    
    func applyCircleShadow(shadowRadius: CGFloat = 6,
                           shadowOpacity: Float = 0.3,
                           shadowColor: CGColor = UIColor.black.cgColor,
                           shadowOffset: CGSize = CGSize.zero) {
        layer.cornerRadius = frame.size.height / 2
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
    }
    
    @IBInspectable var isCircleMe: Bool {
        get {
            return self.isCircleMe
        }
        set {
            if newValue == true {
                let radius = self.bounds.width / 2
                self.layer.cornerRadius = radius
                self.layer.masksToBounds = true
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var shadowColor: UIColor {
        get {
            return self.shadowColor
        }
        set {
            self.layer.shadowColor = newValue.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.layer.shadowOpacity = 0.6
            self.layer.shadowRadius = 1.0
        }
    }

    
    @IBInspectable var BorderColor: UIColor {
        get {
            return UIColor.init(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var BorderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            //view.layer.cornerRadius = view.frame.height * 0.45
            return GISTUtility.convertToRatio(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = GISTUtility.convertToRatio(newValue)
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    func addShadow(shadowColor: CGColor = UIColor.lightGray.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0, height: 0.5),
                   shadowOpacity: Float = 0.6,
                   shadowRadius: CGFloat = 1) {
        
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = UIFont(name: "Quicksand-Bold", size: 13)!
        placeholderLabel.textColor = UIColor.black
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    //Spacing Top ** UITextView **
    @IBInspectable var TopTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.top
        }
        set {
            self.textContainerInset = UIEdgeInsets(top: newValue, left: self.textContainerInset.left, bottom: self.textContainerInset.bottom, right: self.textContainerInset.right)
        }
    }
    
    //Spacing Left ** UITextView **
    @IBInspectable var LeftTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.left
        }
        set {
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: newValue, bottom: self.textContainerInset.bottom, right: self.textContainerInset.right)
        }
    }
    
    //Spacing Bottom ** UITextView **
    @IBInspectable var BottomTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.bottom
        }
        set {
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: self.textContainerInset.left, bottom: newValue, right: self.textContainerInset.right)
        }
    }
    
    //Spacing Right ** UITextView **
    @IBInspectable var RightTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.right
        }
        set {
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: self.textContainerInset.left, bottom: self.textContainerInset.bottom, right: newValue)
        }
    }
    
}

