//
//  Protocols.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}

protocol AlertViewDelegate {
    func okAction()
}

protocol AlertViewDelegateAction {
    func okButtonAction()
    func cancelAction()
}
